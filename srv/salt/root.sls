{% set root = salt['pillar.get']('root', default={}, merge=True) %}

root:
  user.present:
    {% if grains['kernel'] == 'FreeBSD' %}
    - shell: /usr/local/bin/fish
    {% else %}
    - shell: /usr/bin/fish
    {% endif %}
    {% if root.password %}
    - password: {{ root.password }}
    {% endif %}
    - require:
      - pkg: fish
