apache2:
  pkg.installed: []
  service.running:
    - require:
      - file: /srv/www/htdocs/index.html

/srv/www/htdocs/index.html:
  file.managed:
    - source: salt://web/index.html
    - user: root
    - group: root
    - chmod: 0644
    - makedirs: True

