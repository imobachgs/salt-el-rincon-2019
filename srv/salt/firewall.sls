firewalld:
  pkg.installed: []
  service.running: []

public:
  firewalld.present:
    - block_icmp:
        - echo-reply
        - echo-request
    - services:
        - dhcpv6-client
        - http
        - https
        - ssh