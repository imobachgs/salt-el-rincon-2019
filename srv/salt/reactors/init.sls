reactor.conf:
  file.managed:
    - name: /etc/salt/master.d/reactor.conf
    - makedirs: True
    - contents: |
        reactor:
          - 'salt/beacon/*/service/*':
            - /srv/reactor/service.sls
    - user: root
    - group: root
    - mode: 0644

/srv/reactor/service.sls:
  file.managed:
    - source: salt://reactors/service.sls
    - makedirs: True
    - user: root
    - group: root
    - chmod: 0644
