{% if data[data["service_name"]]["running"] == false %}
send service alert to telegram:
  local.telegram.post_message:
    - tgt: {{ data['id'] }}
    - kwarg:
        message: "{{ data['id'] }}: {{ data['service_name'] }} is not running."
{% endif %}
