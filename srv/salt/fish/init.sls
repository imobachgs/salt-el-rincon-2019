fish:
  pkg.installed

fish_variables.conf:
  file.managed:
    - source: salt://fish/variables.fish
    - user: root
    - chmod: 0644
    {% if grains['kernel'] == 'FreeBSD' %}
    - name: /usr/local/etc/fish/conf.d/variables.fish
    - group: wheel
    {% else %}
    - name: /etc/fish/conf.d/variables.fish
    - group: root
    {% endif %}
