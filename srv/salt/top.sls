base:
  '*':
    - fish
    - root
    - tools
  'G@kernel:Linux and minion*':
    - beacons
    - firewall
    - web
  'salt':
    - reactors
